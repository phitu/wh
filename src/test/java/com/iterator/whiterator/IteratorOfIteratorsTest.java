package com.iterator.whiterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class IteratorOfIteratorsTest {

	@Test
	public void iteratorOutputsInCorrectOrder() {
		List<Integer> expected = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9 ,10, 11));
		List<Integer> actual = new ArrayList<>();
		
		List<Integer> list1 = new ArrayList<>(Arrays.asList(1,4,6,7));
		List<Integer> list2 = new ArrayList<>(Arrays.asList(2,8,11));
		List<Integer> list3 = new ArrayList<>(Arrays.asList(3,5,9));
		List<Integer> list4 = new ArrayList<>(Arrays.asList(10));
		List<Iterator<Integer>> combined = new ArrayList<>();
		combined.add(list1.iterator());
		combined.add(list2.iterator());
		combined.add(list3.iterator());
		combined.add(list4.iterator());
		IteratorOfIterators ioi = new IteratorOfIterators(combined.iterator());
		while (ioi.hasNext()) {
			actual.add(ioi.next());
		}
		Assert.assertEquals(expected, actual);
	}
	

	@Test
	public void iteratorOutputsInCorrectOrderIfIteratorIsEmpty() {
		List<Integer> expected = new ArrayList<>(Arrays.asList(1, 3, 4));
		List<Integer> actual = new ArrayList<>();
		
		List<Integer> list1 = new ArrayList<>(Arrays.asList(3,4,1));
		List<Integer> list2 = new ArrayList<>(Arrays.asList());
		List<Iterator<Integer>> combined = new ArrayList<>();
		combined.add(list1.iterator());
		combined.add(list2.iterator());
		IteratorOfIterators ioi = new IteratorOfIterators(combined.iterator());
		while (ioi.hasNext()) {
			actual.add(ioi.next());
		}
		Assert.assertEquals(expected, actual);
	}


	@Test(expected = DuplicateNumberEntryException.class)
	public void exceptionThrownForDuplicateEntry() {
		
		List<Integer> list1 = new ArrayList<>(Arrays.asList(3,4,1));
		List<Integer> list2 = new ArrayList<>(Arrays.asList(3));
		List<Iterator<Integer>> combined = new ArrayList<>();
		combined.add(list1.iterator());
		combined.add(list2.iterator());
		IteratorOfIterators ioi = new IteratorOfIterators(combined.iterator());
		ioi.hasNext();
	}
}
