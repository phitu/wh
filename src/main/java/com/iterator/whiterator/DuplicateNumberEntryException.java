package com.iterator.whiterator;

public class DuplicateNumberEntryException extends RuntimeException {

	public DuplicateNumberEntryException(String message) {
		super(message);
	}
}
