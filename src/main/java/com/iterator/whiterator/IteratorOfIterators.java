package com.iterator.whiterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class IteratorOfIterators implements Iterator<Integer> {
	private Iterator<Iterator<Integer>> iteratorOfIterator;
	private Iterator<Integer> iteratorArr;
	private Iterator<Integer> iteratorValues;

	public IteratorOfIterators(Iterator<Iterator<Integer>> iterators) {
		this.iteratorOfIterator = iterators;
	}

	@Override
	public boolean hasNext() {
		if (iteratorValues == null) {
			List<Integer> intVals = new ArrayList<>();
			while (iteratorOfIterator.hasNext()) {
				iteratorArr = iteratorOfIterator.next();
				while (iteratorArr.hasNext()) {
					intVals.add(iteratorArr.next());
				}
			}
			
			checkForDuplicateEntries(intVals);
			
			Collections.sort(intVals);
			iteratorValues = intVals.iterator();
		}
		while (iteratorValues.hasNext())
			return true;
		return false;
	}

	private void checkForDuplicateEntries(List<Integer> intVals) {
		Set<Integer> dups = intVals.stream().filter(i -> Collections.frequency(intVals, i) > 1).collect(Collectors.toSet());
		if (dups.size() > 0) {
			throw new DuplicateNumberEntryException("Duplicate entry");
		}
	}

	@Override
	public Integer next() {
		return iteratorValues.next();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
